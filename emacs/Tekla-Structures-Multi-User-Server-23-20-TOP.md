## Tekla Structures Multi User Server 23 20

 
 " width="300">
 
 
**## Links to download files:
[Link 1](https://geags.com/2tE7YN)

[Link 2](https://urllie.com/2tE7YN)

[Link 3](https://tiurll.com/2tE7YN)

**

 
 
 
 
 
# How to Set Up and Use Tekla Structures Multi User Server 23 20
 
Tekla Structures is a powerful software for structural design and detailing. It allows multiple users to work on the same model simultaneously, using the multi-user mode. This mode requires a server computer that runs the Tekla Structures Multi User Server 23 20 service, and a file server computer that stores the master model. In this article, we will show you how to install and configure the multi-user server, and how to convert and open models in the multi-user mode.
 
## Installing and Configuring the Multi-User Server
 
The first step is to download the Tekla Structures Multi User Server 23 20 installation package from [Tekla Downloads](https://download.tekla.com/). This package is compatible with Tekla Structures versions 17.0 and later. You should install it on a server computer that is accessible by all the users who want to work on the same model. The default installation folder is C:\Program Files (x86)\Tekla Structures Multi-user Server.
 
After installing the package, you need to run the xs\_server.exe file inside the installation folder. This will start the multi-user server service in the background. You can check the status of the service in the Windows Services panel (Control Panel > Administrative Tools > Services). If you encounter any error while running the xs\_server.exe file, try to run it as an administrator.
 
The multi-user server service uses TCP/IP protocol to communicate with the client computers. You need to make sure that the server computer has a valid IP address, and that the port number 1245 is not blocked by any firewall or antivirus software. You can also add the xs\_server.exe file to the exception list of your firewall or antivirus software.
 
## Converting and Opening Models in Multi-User Mode
 
The next step is to convert a single-user model to a multi-user model. The model must be located in a network directory that is accessible by all the users who want to work on it. You can convert any model that is open or closed in Tekla Structures.
 
To convert an open model, go to File > Sharing > Convert to a multi-user model. To convert a closed model, go to File > Open > All models, select the model from the list, and click Convert to a multi-user model. In both cases, you will be prompted to enter or select the multi-user server name or IP address in the Convert to multi-user model dialog box. Click Convert to finish the process.
 
To open a multi-user model, go to File > Open > All models, select the model from the list, and click Open in multi-user mode. You can also double-click on the model name if it has a green icon indicating that it is a multi-user model. You will be connected to the multi-user server and join other users who are working on the same model.
 
## Benefits and Best Practices of Multi-User Mode
 
The multi-user mode offers several benefits for collaborative work on complex models. It allows users to share information and changes in real time, without creating conflicts or duplicates. It also reduces the risk of data loss or corruption, as all changes are saved on the master model on the file server computer.
 
However, there are some best practices that you should follow to ensure a smooth and efficient multi-user experience. These include:
 
- Using the same Tekla Structures version, service pack, environment, role, and configuration/license type for all users who work on the same model.
- Keeping your local cache folder clean and up-to-date by using the File > Sharing > Synchronize command regularly.
- Using descriptive names for your objects and groups, and avoiding deleting or renaming objects that are used by other users.
- Using filters and selection switches to control what you see and modify in your view.
- Using locks and reservations to prevent other users from modifying objects that you are working on.
- Using comments and notes to communicate with other users about your changes or issues.
- Using reports and logs to monitor and track your changes and those of other users.

For more information about these features and functions, please refer to [Tekla](https://support.tekla.com/)
 dde7e20689
 
 
